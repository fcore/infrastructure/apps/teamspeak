# teamspeak

Repository containing Kubernetes Mantifests for Teamspeak, a voice chat system.

### Notes:

#### EULA
You must read and accept the [TS3 EULA](https://github.com/TeamSpeak-Systems/teamspeak-linux-docker-images/blob/master/LICENSE) before executing the server software. 
Additionally, the following environment variable on the teamspeak container must be set in order for the server to start.

```yaml
env:
- name: TS3SERVER_LICENSE
    value: "accept"
```

#### IP Address Setup
When creating the services, you should attempt to ensure that both services end up on the same VIP. File Transfer service can/will break if they exist on seperate IP addresses.

#### ServerAdmin Token
The pod will spit out the ServerAdmin token (used for getting ServerAdmin on a client) on the first boot. If you don't get the token on the first attempt, clear out the PVC and try again.